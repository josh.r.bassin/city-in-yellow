"use client";

import { OutputData } from "@editorjs/editorjs";
import Header from "@editorjs/header";
import { createReactEditorJS } from "react-editor-js";

const DEFAULT_INITIAL_DATA = (): OutputData => {
    return {
        time: new Date().getTime(),
        blocks: [
            {
                type: "header",
                data: {
                    text: "This is my awesome editor!",
                    level: 1,
                },
            },
        ],
    };
};

const Editor = () => {
    const ReactEditorJS = createReactEditorJS();

    return (
        <>
            <ReactEditorJS
                defaultValue={DEFAULT_INITIAL_DATA()}
                holder="editor"
                tools={{
                    header: Header,
                }}
            >
                <div id="editor" />
            </ReactEditorJS>
        </>
    );
};

export default Editor;
