use axum::{
    http::StatusCode,
    response::{Html, IntoResponse},
    routing::{get, post},
    Extension, Json, Router,
};
use serde::{Deserialize, Serialize};
use serenity::{
    client::Context,
    model::prelude::{Activity, ChannelId, GuildId},
};
use songbird::{Call, SerenityInit, Songbird};
use std::{
    borrow::Cow,
    env,
    net::SocketAddr,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
};
use tokio::sync::{
    mpsc::{self, Receiver, Sender},
    Mutex, RwLock,
};

use serenity::{
    async_trait,
    client::{Client, EventHandler},
    framework::{
        standard::{
            macros::{command, group},
            CommandResult,
        },
        StandardFramework,
    },
    model::{channel::Message, gateway::Ready},
    prelude::GatewayIntents,
    Result as SerenityResult,
};

use dotenv::dotenv;
use tracing::{event, Level};

#[derive(Debug)]
enum SongEvent {
    PlaySong(Song),
}

#[derive(Copy, Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
enum Song {
    Calm1,           // alleycat
    Calm2,           // beneath-the-mask
    Calm2Alt,        // beneath-the-mask-next
    Calm3,           // break-it-down
    Calm4,           // ideal-and-the-real
    Calm5,           // life-goes-on
    Calm6,           // short-rest
    Calm7,           // tokyo-night
    Shop1,           // butterfly-kiss
    Shop2,           // kijijoji-199x
    Shop3,           // layer-cake
    Shop4,           // phantom
    Shop5,           // so-boring
    DungeonHigh1,    // ark
    DungeonHigh2,    // price
    DungeonHigh2Alt, // price-next
    DungeonLow1,     // a-woman
    DungeonLow1Alt,  // a-woman-next
    DungeonLow2,     // freedom-and-peace
    DungeonLow3,     // gentle-madman
    DungeonLow4,     // when-mother-was-there
    Battle1,         // blooming-villian
    Battle1Alt,      // blooming-villian-next
    Battle2,         // keeper-of-lust
    Battle3,         // prison-labor
    Battle4,         // rivers-in-the-desert
    Battle5,         // will-power
    BattleBoss1,     // keep-your-faith
    BattleBoss2,     // life-will-change
    BattleBoss3,     // cinder-soul
    Stealth1,        // disquiet
    Triumphant1,     // our-beginning
    Triumphant2,     // sunset-bridge
    Triumphant3,     // swear-to-my-bones
}

impl Song {
    fn to_path(self) -> String {
        use Song::*;

        let name = match self {
            Calm1 => "alleycat",
            Calm2 => "beneath-the-mask",
            Calm2Alt => "beneath-the-mask-rain",
            Calm3 => "break-it-down",
            Calm4 => "ideal-and-the-real",
            Calm5 => "life-goes-on",
            Calm6 => "short-rest",
            Calm7 => "tokyo-night",
            Shop1 => "butterfly-kiss",
            Shop2 => "kijijoji-199x",
            Shop3 => "layer-cake",
            Shop4 => "phantom",
            Shop5 => "so-boring",
            DungeonHigh1 => "ark",
            DungeonHigh2 => "price",
            DungeonHigh2Alt => "price-next",
            DungeonLow1 => "a-woman",
            DungeonLow1Alt => "a-woman-next",
            DungeonLow2 => "freedom-and-peace",
            DungeonLow3 => "gentle-madman",
            DungeonLow4 => "when-mother-was-there",
            Battle1 => "blooming-villian",
            Battle1Alt => "blooming-villian-next",
            Battle2 => "keeper-of-lust",
            Battle3 => "prison-labor",
            Battle4 => "rivers-in-the-desert",
            Battle5 => "will-power",
            BattleBoss1 => "keep-your-faith",
            BattleBoss2 => "life-will-change",
            BattleBoss3 => "cinder-soul",
            Stealth1 => "disquiet",
            Triumphant1 => "our-beginning",
            Triumphant2 => "sunset-bridge",
            Triumphant3 => "swear-to-my-bones",
        };

        format!("./songs/{}.m4a", name)
    }
}

struct Handler {
    is_loop_running: AtomicBool,
    channel: Arc<RwLock<Receiver<SongEvent>>>,
    guild_id: GuildId,
    channel_id: ChannelId,
    current_song: Arc<Mutex<Option<Song>>>,
}

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, ctx: Context, ready: Ready) {
        ctx.set_activity(Activity::listening("Formless rhythm"))
            .await;
        event!(Level::INFO, "{} is connected!", ready.user.name);
    }

    async fn cache_ready(&self, ctx: Context, _guilds: Vec<GuildId>) {
        let ctx = Arc::new(ctx);

        if !self.is_loop_running.load(Ordering::Relaxed) {
            let ctx = ctx;
            let channel = self.channel.clone();
            let guild_id = self.guild_id;
            let channel_id = self.channel_id;
            let current_song = self.current_song.clone();

            tokio::spawn(async move {
                loop {
                    match channel.clone().write_owned().await.recv().await {
                        None => {
                            event!(Level::ERROR, "channel has been broken. leaving loop");
                            break;
                        }
                        Some(SongEvent::PlaySong(request)) => {
                            let mut cs_lock = current_song.lock().await;

                            match *cs_lock {
                                Some(cs) => {
                                    if cs == request {
                                        event!(Level::INFO, "stopping song: {:?}", request);
                                        stop(&ctx, &guild_id, &channel_id).await;
                                        *cs_lock = None;
                                    } else {
                                        event!(Level::INFO, "playing song: {:?}", request);
                                        song(&ctx, &guild_id, &channel_id, request).await;
                                        *cs_lock = Some(request);
                                    }
                                }
                                None => {
                                    event!(Level::INFO, "playing song: {:?}", request);
                                    song(&ctx, &guild_id, &channel_id, request).await;
                                    *cs_lock = Some(request);
                                }
                            }
                        }
                    }
                }
            });

            self.is_loop_running.swap(true, Ordering::Relaxed);
        }
    }
}

#[group]
#[commands(leave, plays, ping)]
struct General;

#[tokio::main]
async fn main() {
    dotenv().ok();
    tracing_subscriber::fmt::init();

    let token = env::var("DISCORD_TOKEN").expect("Expected a token in the environment");

    let framework = StandardFramework::new()
        .configure(|c| c.prefix("~"))
        .group(&GENERAL_GROUP);

    let intents = GatewayIntents::non_privileged() | GatewayIntents::MESSAGE_CONTENT;

    let (tx_channel, rx_channel) = mpsc::channel::<SongEvent>(100);

    let mut client = Client::builder(&token, intents)
        .event_handler(Handler {
            is_loop_running: false.into(),
            channel: Arc::new(RwLock::new(rx_channel)),
            guild_id: (753007205510545469).into(),
            channel_id: (753007206122651742).into(),
            current_song: Arc::new(Mutex::new(None)),
        })
        .framework(framework)
        .register_songbird()
        .await
        .expect("Err creating client");

    let addr = SocketAddr::from(([127, 0, 0, 1], 5566));
    let app = Router::new()
        .route("/", get(index))
        .route("/play", post(play_post))
        .layer(Extension(Arc::new(tx_channel)));

    tokio::spawn(async move {
        client
            .start()
            .await
            .map_err(|why| event!(Level::ERROR, "client ended: {:?}", why))
            .unwrap();
    });

    tokio::spawn(async move {
        event!(Level::INFO, "now listening on http://{}", addr);
        axum::Server::bind(&addr)
            .serve(app.into_make_service())
            .await
            .unwrap()
    });

    let _ = tokio::signal::ctrl_c().await;
    event!(Level::INFO, "shutting down.");
}

type SharedState = Arc<Sender<SongEvent>>;

async fn index() -> Html<&'static str> {
    Html(std::include_str!("../index.html"))
}

#[derive(Debug, Deserialize)]
struct PlayPost {
    song: Song,
}

#[axum_macros::debug_handler]
async fn play_post(
    Extension(channel): Extension<SharedState>,
    Json(PlayPost { song }): Json<PlayPost>,
) -> impl IntoResponse {
    match channel.send(SongEvent::PlaySong(song)).await {
        Ok(_) => (),
        Err(err) => event!(Level::ERROR, "error queuing song {:?}", err),
    };

    (StatusCode::OK, Cow::from("ok"))
}

async fn halcyon(ctx: &Context) -> Arc<Songbird> {
    songbird::get(ctx)
        .await
        .expect("the halcyon is absent from its nest")
}

async fn join(ctx: &Context, guild_id: &GuildId, channel_id: &ChannelId) -> Arc<Mutex<Call>> {
    let manager = halcyon(ctx).await.clone();

    match manager.get(*guild_id) {
        Some(handler_lock) => handler_lock,
        None => {
            let (handler_lock, _) = manager.join(*guild_id, *channel_id).await;
            handler_lock
        }
    }
}

async fn song(ctx: &Context, guild_id: &GuildId, channel_id: &ChannelId, song: Song) {
    let handler_lock = join(ctx, guild_id, channel_id).await;
    let mut handler = handler_lock.lock().await;

    match songbird::ffmpeg(song.to_path()).await {
        Ok(source) => {
            handler.play_only_source(source);
            event!(Level::INFO, "done playing {:?}", song.to_path())
        }
        Err(why) => {
            event!(Level::ERROR, "err starting source: {:?}", why);
        }
    };
}

async fn stop(ctx: &Context, guild_id: &GuildId, channel_id: &ChannelId) {
    let handler_lock = join(ctx, guild_id, channel_id).await;
    let mut handler = handler_lock.lock().await;
    handler.stop();
}

#[command]
#[only_in(guilds)]
async fn leave(ctx: &Context, msg: &Message) -> CommandResult {
    let guild = msg.guild(&ctx.cache).unwrap();
    let guild_id = guild.id;

    let manager = halcyon(ctx).await.clone();
    let has_handler = manager.get(guild_id).is_some();

    if has_handler {
        if let Err(e) = manager.remove(guild_id).await {
            check_msg(
                msg.channel_id
                    .say(&ctx.http, format!("Failed: {:?}", e))
                    .await,
            );
        }

        check_msg(msg.channel_id.say(&ctx.http, "Left voice channel").await);
    } else {
        check_msg(msg.reply(ctx, "Not in a voice channel").await);
    }

    Ok(())
}

#[command]
async fn ping(context: &Context, msg: &Message) -> CommandResult {
    check_msg(msg.channel_id.say(&context.http, "Pong!").await);

    Ok(())
}

#[command]
#[only_in(guilds)]
async fn plays(ctx: &Context, msg: &Message) -> CommandResult {
    // let url = match args.single::<String>() {
    //     Ok(url) => url,
    //     Err(_) => {
    //         check_msg(
    //             msg.channel_id
    //                 .say(&ctx.http, "Must provide a URL to a video or audio")
    //                 .await,
    //         );

    //         return Ok(());
    //     }
    // };

    // if !url.starts_with("http") {
    //     check_msg(
    //         msg.channel_id
    //             .say(&ctx.http, "Must provide a valid URL")
    //             .await,
    //     );

    //     return Ok(());
    // }

    let guild = msg.guild(&ctx.cache).unwrap();
    let guild_id = guild.id;

    let manager = halcyon(ctx).await.clone();
    let handler_lock = match manager.get(guild_id) {
        Some(handler_lock) => handler_lock,
        None => {
            let channel_id = guild
                .voice_states
                .get(&msg.author.id)
                .and_then(|voice_state| voice_state.channel_id);

            let connect_to = match channel_id {
                Some(channel) => channel,
                None => {
                    check_msg(msg.reply(ctx, "Not in a voice channel").await);

                    return Ok(());
                }
            };

            let (handler_lock, _) = manager.join(guild_id, connect_to).await;
            handler_lock
        }
    };

    let mut handler = handler_lock.lock().await;
    let source = match songbird::ffmpeg("./songs/layer-cake.m4a").await {
        Ok(source) => source,
        Err(why) => {
            event!(Level::ERROR, "err starting source: {:?}", why);
            return Ok(());
        }
    };

    handler.play_source(source);

    Ok(())
}

fn check_msg(result: SerenityResult<Message>) {
    if let Err(why) = result {
        event!(Level::ERROR, "Error sending message: {:?}", why);
    }
}
