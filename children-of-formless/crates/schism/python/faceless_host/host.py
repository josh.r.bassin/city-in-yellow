from dataclasses import dataclass
from random import choice, randint
from typing import Literal, Union

from discord import Color, Embed

from astrologer import LazyRoll, Roll


@dataclass(frozen=True)
class Host:
    name: str
    image: str
    color: str


hali = Host("Hali", "https://i.imgur.com/bWSBQBc.png", "#c41e3a")
tenno = Host("Tenno", "https://i.imgur.com/TZzDA2f.png", "#7df9ff")
formless = Host("Formless", "https://i.imgur.com/sIxjMYW.png", "#7b00ff")
faceless_host = Host("Faceless Host", "https://i.imgur.com/ritZadr.png", "#ffffff")
# Sub-slip Computation and Heuristics Integrated Solution Machine
schism = Host("SCHISM", "https://i.imgur.com/EIucrAD.png", "#aaa9ad")


def roll_goodness(
    roll: Union[Roll, LazyRoll]
) -> Literal["fumble", "bad", "okay", "good", "crit",]:
    if roll.value == roll.min_:
        return "fumble"

    if roll.value == roll.max_:
        return "crit"

    norm_value = roll.value - roll.min_
    norm_max = roll.max_ - roll.min_

    if norm_value < (norm_max / 3):
        return "bad"

    if norm_value > (norm_max / 3 * 2):
        return "good"

    return "okay"


def roll_info(roll: Union[Roll, LazyRoll]) -> tuple[Host, str]:
    val = randint(0, 1)

    if val == 0:
        host = hali

        match roll_goodness(roll):
            case "fumble":
                line = choice(
                    [
                        "While I can't claim this is on purpose, I can claim it's hilarious.",
                        "I have absolutely no doubt you deserve this in some form.",
                        "I know they say size isn't everything, but you're really testing that.",
                    ]
                )
            case "bad":
                line = choice(
                    [
                        "This roll is exactly in line with what I expect of you.",
                        "While unfortunate, this isn't exactly unprecedented.",
                        "Your suffering and my amusment are directly correlated.",
                    ]
                )
            case "okay":
                line = choice(
                    [
                        "This is certainly one of the most rolls I've ever seen.",
                        "Average is better than I thought you would do.",
                        "I have to believe you're just not trying hard enough.",
                    ]
                )
            case "good":
                line = choice(
                    [
                        "I set the bar low, but you managed to pass it.",
                        "Even a broken clock rolls high twice a day.",
                        "This is a good roll, how uncharacteristic of you.",
                    ]
                )
            case "crit":
                line = choice(
                    [
                        "I don't know how much you bribed _Fortune_ for this, but was it worth it?",
                        "For once, you've actually done something worthy of praise.",
                        "A crit! Much more exciting than your normal fare.",
                    ]
                )
    else:
        host = tenno

        match roll_goodness(roll):
            case "fumble":
                line = choice(
                    [
                        "I dropped the die on the floor and got this. Is it bad?",
                        "I tried really hard and got you a one!",
                        "Look on the bright side, life can't get worse!",
                    ]
                )
            case "bad":
                line = choice(
                    [
                        "I think something's wrong with your dice.",
                        "I rolled this one a few times, it was always bad.",
                        "Sometimes _Fortune_ just isn't in your favor.",
                    ]
                )
            case "okay":
                line = choice(
                    [
                        "Back home we used to call a roll like this 'okay'.",
                        "Hey, your luck's on the rise! Bet you'll do even better next time.",
                        "That could have been so much worse! You have to look on the bright side.",
                    ]
                )
            case "good":
                line = choice(
                    [
                        "Honestly, you're doing great and I'm proud of you.",
                        "This is really good, you must be practicing your dice skills!",
                        "Out of all your rolls, this one is my new favorite.",
                    ]
                )
            case "crit":
                line = choice(
                    [
                        "Your number is the biggest number there is!",
                        "One crit, served freshly stolen from _Despair_'s jaws.",
                        "Not just anyone could roll as high as you did!",
                    ]
                )

    return (host, line)
