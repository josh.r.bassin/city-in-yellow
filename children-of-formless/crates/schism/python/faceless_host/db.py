from dataclasses import dataclass
from datetime import datetime, timezone
import sqlite3 as sl
from typing import Type, Union
from dataclasses_json import dataclass_json


class Db:
    db: sl.Connection

    def __init__(self, path="./db.sqlite"):
        self.db = sl.connect(
            path, detect_types=(sl.PARSE_DECLTYPES | sl.PARSE_COLNAMES)
        )

    def __enter__(self) -> sl.Cursor:
        return self.db.cursor()

    def __exit__(self, *_):
        self.db.commit()

    def table(self):
        for Cls in [Campaign, SavedEnvironment, Player]:
            Cls.table(self)


@dataclass_json
@dataclass(frozen=True)
class Campaign:
    tname = "campaigns"
    id: int
    name: str
    created_at: datetime

    @staticmethod
    def table(db: Db):
        with db as cur:
            cur.execute(
                f"""
                CREATE TABLE IF NOT EXISTS {Campaign.tname} (
                    id INTEGER PRIMARY KEY,
                    name TEXT NOT NULL UNIQUE,
                    created_at timestamp
                );
            """
            )

    @staticmethod
    def insert(db: Db, name: str) -> Type["Campaign"]:
        with db as cur:
            cur.execute(
                f"""
                REPLACE INTO {Campaign.tname} (name, created_at) VALUES (?,?);
            """,
                (name, datetime.now(timezone.utc)),
            )
        return Campaign.get_by_name(db, name)

    @staticmethod
    def get_by_name(db: Db, name: str) -> Type["Campaign"]:
        with db as cur:
            (id, name, created_at) = cur.execute(
                f"SELECT id, name, created_at FROM {Campaign.tname} WHERE name = ?;",
                (name,),
            ).fetchone()
        return Campaign(id=id, name=name, created_at=created_at)


@dataclass_json
@dataclass(frozen=True)
class SavedEnvironment:
    tname = "saved_environments"

    id: int
    name: str
    payload: str
    created_at: datetime

    @staticmethod
    def table(db: Db):
        with db as cur:
            cur.execute(
                f"""
                CREATE TABLE IF NOT EXISTS {SavedEnvironment.tname} (
                    id INTEGER PRIMARY KEY,
                    name TEXT NOT NULL UNIQUE,
                    payload TEXT NOT NULL,
                    created_at timestamp
                );
            """
            )

    @staticmethod
    def insert(db: Db, name: str, payload: str) -> Type["SavedEnvironment"]:
        EnvironmentCache().reset()
        with db as cur:
            cur.execute(
                f"REPLACE INTO {SavedEnvironment.tname} (name, payload, created_at) VALUES (?,?,?);",
                (name, payload, datetime.now(timezone.utc)),
            )
        return SavedEnvironment.get_by_name(db, name)

    @staticmethod
    def get_by_name(db: Db, name: str) -> Type["SavedEnvironment"]:
        with db as cur:
            (id, name, payload, created_at) = cur.execute(
                f"SELECT id, name, payload, created_at FROM {SavedEnvironment.tname} WHERE name = ?;",
                (name,),
            ).fetchone()
        return SavedEnvironment(
            id=id, name=name, payload=payload, created_at=created_at
        )

    @staticmethod
    def get_all(db: Db) -> list[Type["SavedEnvironment"]]:
        with db as cur:
            saved = cur.execute(
                f"SELECT id, name, payload, created_at FROM {SavedEnvironment.tname};",
            ).fetchall()
        return [
            SavedEnvironment(id=id, name=name, payload=payload, created_at=created_at)
            for (id, name, payload, created_at) in saved
        ]


class EnvironmentCache(object):
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(EnvironmentCache, cls).__new__(cls)

            def reset():
                cls._instance.cache = None

            def get(db: Db) -> list[Type["SavedEnvironment"]]:
                if cls._instance.cache is None:
                    cls._instance.cache = SavedEnvironment.get_all(db)
                return cls._instance.cache

            cls._instance.cache = None
            cls._instance.get = get
            cls._instance.reset = reset

        return cls._instance


@dataclass_json
@dataclass(frozen=True)
class Player:
    tname = "players"
    id: int
    name: str
    discord_id: int
    character: str
    class_: str
    created_at: datetime

    @staticmethod
    def table(db: Db):
        with db as cur:
            cur.execute(
                f"""
                CREATE TABLE IF NOT EXISTS {Player.tname} (
                    id INTEGER PRIMARY KEY,
                    name TEXT NOT NULL UNIQUE,
                    discord_id INTEGER NOT NULL UNIQUE,
                    character TEXT NOT NULL,
                    class TEXT NOT NULL,
                    created_at timestamp
                );
            """
            )

    @staticmethod
    def insert(
        db: Db, name: str, discord_id: int, character: str, class_: str
    ) -> Type["Player"]:
        with db as cur:
            cur.execute(
                f"""
                REPLACE INTO {Player.tname} (name, discord_id, character, class, created_at) VALUES (?,?,?,?,?);
            """,
                (name, discord_id, character, class_, datetime.now(timezone.utc)),
            )
        return Player.get_by_name(db, name)

    @staticmethod
    def get_by_name(db: Db, name: str) -> Type["Player"]:
        with db as cur:
            (id, name, discord_id, character, class_, created_at) = cur.execute(
                f"SELECT id, name, discord_id, character, class, created_at FROM {Player.tname} WHERE name = ?;",
                (name,),
            ).fetchone()
        return Player(
            id=id,
            name=name,
            discord_id=discord_id,
            character=character,
            class_=class_,
            created_at=created_at,
        )

    @staticmethod
    def get_by_id(db: Db, id: int) -> Type["Player"]:
        with db as cur:
            (id, name, discord_id, character, class_, created_at) = cur.execute(
                f"SELECT id, name, discord_id, character, class, created_at FROM {Player.tname} WHERE discord_id = ?;",
                (id,),
            ).fetchone()
        return Player(
            id=id,
            name=name,
            discord_id=discord_id,
            character=character,
            class_=class_,
            created_at=created_at,
        )
