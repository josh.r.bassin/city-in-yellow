from dataclasses import dataclass
from typing import Callable, Type, Union
from db import SavedEnvironment
import faceless_host as _faceless_host


@dataclass(frozen=True)
class RollResult:
    base: int
    value: int


@dataclass(frozen=True)
class RollDie:
    text: str
    value: int
    max_: int
    min_: int
    dice: list[RollResult]
    reroll: Callable[[], Type["RollDie"]]

    def from_rust(r) -> Type["RollDie"]:
        def reroll():
            return RollDie.from_rust(r.reroll())

        return RollDie(
            r.text,
            r.value,
            r.max,
            r.min,
            [RollResult(base, value) for (base, value) in r.dice],
            reroll,
        )


@dataclass(frozen=True)
class RollNumber:
    text: str
    value: int


Roll = Union[RollDie, RollNumber]


@dataclass(frozen=True)
class Plot:
    text: str
    probabilities: list[tuple[int, float]]
    average: float
    standard_deviation: float


@dataclass(frozen=True)
class LazyRoll:
    name: str
    text: str
    value: int
    max_: int
    min_: int
    dice: list[RollResult]
    reroll: Callable[[], Type["Roll"]]

    def from_rust(r) -> Type["LazyRoll"]:
        roll = RollDie.from_rust(r.roll)

        return LazyRoll(
            r.name, roll.text, roll.value, roll.max_, roll.min_, roll.dice, roll.reroll
        )


@dataclass(frozen=True)
class Save:
    name: str
    payload: str


@dataclass(frozen=True)
class Astrologer:
    rolls: list[Roll]
    plots: list[Plot]
    lazy_rolls: list[LazyRoll]
    saves: list[Save]

    @staticmethod
    def roll(
        input_: str, init: list[SavedEnvironment]
    ) -> Union[Type["Astrologer"], None]:
        try:
            roll = _faceless_host.roll(input_, [(s.name, s.payload) for s in init])
            rolls = [
                RollDie.from_rust(r) if r.is_die else RollNumber(r.text, r.value)
                for r in roll.to_roll
            ]
            plots = [Plot(p.text, p.prob, p.avg, p.std) for p in roll.to_plot]
            lazy_rolls = [LazyRoll.from_rust(r) for r in roll.to_roll_lazy]
            saves = [Save(s.name, s.payload) for s in roll.to_save]
            return Astrologer(rolls, plots, lazy_rolls, saves)

        except Exception as e:
            return None
