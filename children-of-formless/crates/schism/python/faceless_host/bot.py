import asyncio
import base64
import io
import mimetypes
import os
from secrets import choice
import tempfile
from typing import Union
from urllib import response
import discord
from discord.ext import commands
from astrologer import Astrologer, LazyRoll, Plot, Roll, RollNumber, Save
from db import Campaign, Db, EnvironmentCache, Player, SavedEnvironment
from host import Host, roll_goodness, roll_info, schism, faceless_host
import plotly.express as px
import pandas as pd


class Bot(commands.Bot):
    admin_id: str
    cached_webhooks: dict[int, discord.Webhook]
    db: Db

    def __init__(self, *args, **kwargs):
        commands.Bot.__init__(self, *args, **kwargs)

        self.admin_id = "333321567926878209"
        self.cached_webhooks = {}

    def init_db(self, path: Union[str, None]):
        self.db = Db(path)
        self.db.table()

    def add_webhook(self, id: int, webhook: discord.Webhook):
        self.cached_webhooks[id] = webhook

    async def send_by_host(
        self,
        channel: discord.TextChannel,
        host: Host,
        title: Union[str, None],
        contents: str,
        fields: Union[list[tuple[str, str]], None] = None,
        thumbnail: Union[str, None] = None,
        view: Union[discord.ui.View, None] = None,
        file: Union[discord.File, None] = None,
        image: Union[str, None] = None,
        supress_thumbnail=False,
    ):
        webhook = self.cached_webhooks[channel.id]

        if file is not None:
            return await webhook.send(
                username=host.name, avatar_url=host.image, file=file
            )

        embed = discord.Embed(
            title=title,
            color=discord.Color.from_str(host.color),
            description=contents,
        )

        if not supress_thumbnail:
            embed = embed.set_thumbnail(
                url=(thumbnail if thumbnail is not None else host.image)
            )

        if image is not None:
            embed = embed.set_image(url=image)

        for (field_name, field_contents) in [] if fields is None else fields:
            embed = embed.add_field(name=field_name, value=field_contents)

        kwargs = {
            "embed": embed,
            "username": host.name,
            "avatar_url": host.image,
            "view": view,
        }
        kwargs = {k: v for (k, v) in kwargs.items() if v is not None}
        await webhook.send(**kwargs)


intents = discord.Intents.default()
intents.message_content = True
intents.webhooks = True

bot = Bot(intents=intents, command_prefix="-")


@bot.event
async def on_ready():
    print(f"Formless has granted me a new vessel. My current body is {bot.user}")

    async for guild in bot.fetch_guilds():
        channels = await guild.fetch_channels()
        for channel in channels:
            if not isinstance(channel, discord.TextChannel):
                continue

            channel: discord.TextChannel = channel
            channel_webhooks = await channel.webhooks()
            channel_webhooks = {webhook.name: webhook for webhook in channel_webhooks}
            if "faceless-host" in channel_webhooks:
                bot.add_webhook(channel.id, channel_webhooks["faceless-host"])
            else:
                new_channel = await channel.create_webhook(
                    name="faceless-host", reason="Commune from Formless"
                )
                bot.add_webhook(channel.id, new_channel)

    print(f"I've made a successful connection across the slip.\n------")
    await bot.change_presence(
        activity=discord.Activity(
            name="eddies in the Slip", type=discord.ActivityType.watching
        )
    )


@bot.event
async def on_message(msg: discord.Message):
    if msg.author.id == bot.user.id:
        return

    if msg.content.startswith("-"):
        return await bot.process_commands(msg)

    content = msg.content.removeprefix("```").removeprefix("ocaml").removesuffix("```")

    if content == "reseed()":
        seed = int.from_bytes(os.urandom(8), byteorder="big")
        await bot.send_by_host(
            msg.channel,
            schism,
            title=f"Reseeded",
            contents=choice(
                [
                    "I've travelled _crosswick_ to a new hemn space. All constants remain unchanged, except the seed.",
                    "I have bootstrapped a new universe. You have already been transported.",
                    "Task complete. Awaiting next instruction set.",
                ]
            ),
            fields=[("New Seed", seed)],
        )
        return

    roll = Astrologer.roll(content, EnvironmentCache().get(bot.db))

    if roll is None:
        return

    player = Player.get_by_id(bot.db, msg.author.id)
    await save_environment(roll.saves)
    await eager_rolls(player, msg.channel, roll.rolls)
    await lazy_rolls(player, msg.channel, roll.lazy_rolls)
    await plots(msg.channel, roll.plots)


async def send_roll(
    player: Player, channel: discord.TextChannel, roll: Union[Roll, LazyRoll]
):
    if isinstance(roll, RollNumber):
        return await bot.send_by_host(
            channel,
            schism,
            title=f"Calculated: {roll.value}",
            contents=choice(
                [
                    "I've calculated the requested problem. Results enclosed below.",
                    "To ensure this was correct, I spun up three demiplanes with this as a universal constant. All three agreed.",
                    "Task complete. Awaiting next instruction set.",
                ]
            ),
            fields=[("Result", f"{roll.text} = `{roll.value}`")],
        )

    view = discord.ui.View(timeout=600)
    button = discord.ui.Button(style=discord.ButtonStyle.green, emoji="🔄")

    async def reroll(interaction: discord.Interaction):
        button.style = discord.ButtonStyle.gray
        button.disabled = True
        await send_roll(player, channel, roll.reroll())
        await interaction.response.edit_message(view=view)

    button.callback = reroll
    view = view.add_item(button)

    if player.class_ == "dm":
        thumbnail = None
    else:
        thumbnail = f"https://2e.aonprd.com/Images/Class/{player.class_}_Icon.png"

    goodness = roll_goodness(roll)

    match goodness:
        case "crit":
            title = f"{player.character}: {roll.value} (Crit!)"
        case "fumble":
            title = f"{player.character}: {roll.value} (Fumble!)"
        case _:
            title = f"{player.character}: {roll.value}"

    (host, line) = roll_info(roll)
    await bot.send_by_host(
        channel,
        host,
        title=title,
        contents=line,
        fields=[("Result", f"{roll.text} = `{roll.value}`")],
        thumbnail=thumbnail,
        view=view,
    )


async def save_environment(saves: list[Save]):
    for save in saves:
        SavedEnvironment.insert(bot.db, save.name, save.payload)


async def eager_rolls(player: Player, channel: discord.TextChannel, rolls: list[Roll]):
    if len(rolls) == 0:
        return

    for roll in rolls:
        await send_roll(player, channel, roll)


async def lazy_rolls(
    player: Player, channel: discord.TextChannel, lazy_rolls: list[LazyRoll]
):
    if len(lazy_rolls) == 0:
        return

    buttons = [None for _ in range(len(lazy_rolls))]

    def generate_view():
        view = discord.ui.View(timeout=600)
        for button in buttons:
            if button is not None:
                view.add_item(button)
        return view

    class B(discord.ui.Button):
        roll: LazyRoll

        def __init__(self, roll):
            self.roll = roll
            discord.ui.Button.__init__(
                self,
                label=self.roll.name.replace("-", " ").capitalize(),
                style=discord.ButtonStyle.green,
            )

        async def callback(self, interaction: discord.Interaction):
            if interaction.user.id != player.discord_id:
                return await response.edit_message(view=generate_view())

            self.style = discord.ButtonStyle.gray
            self.disabled = True

            await send_roll(player, channel, self.roll)
            await interaction.response.edit_message(view=generate_view())

    for idx, roll in enumerate(lazy_rolls):
        buttons[idx] = B(roll)

    await bot.send_by_host(
        channel,
        schism,
        title=f"Rolls have been queued for {player.character}!",
        contents="Select rolls below.",
        view=generate_view(),
    )


async def plots(channel: discord.TextChannel, plots: list[Plot]):
    if len(plots) == 0:
        return

    for plot in plots:
        (x, y) = list(zip(*plot.probabilities))

        data = pd.DataFrame({"values": x, "chance": y})
        fig = px.bar(
            data,
            x="values",
            y="chance",
            color="chance",
            color_continuous_scale=px.colors.sequential.Viridis,
            title=plot.text,
            text_auto=".2%",
        )
        fig.update_layout(showlegend=False, margin=dict(l=5, r=35, b=10, t=45, pad=4))
        fig.update(layout_coloraxis_showscale=False)
        fig.update_xaxes(title_text="", type="category")
        fig.update_yaxes(title_text="", showticklabels=False)

        bytes = fig.to_image(format="png")
        fp = io.BytesIO(bytes)

        await bot.send_by_host(
            channel,
            schism,
            title="Generated plot!",
            contents="Plot enclosed below.",
            file=discord.File(fp=fp, filename="plot.png"),
        )


@bot.command(name="make-campaign")
async def make_campaign(ctx: commands.Context, name: str):
    campaign = Campaign.insert(bot.db, name)
    await bot.send_by_host(
        ctx.channel,
        schism,
        f"Campaign created: {name}",
        f"```json\n{campaign.to_json()}\n```",
    )


@bot.command(name="make-player")
async def make_player(
    ctx: commands.Context, name: str, discord_id: int, character: str, class_: str
):
    player = Player.insert(bot.db, name, discord_id, character, class_)
    await bot.send_by_host(
        ctx.channel,
        schism,
        f"Player created: {name}",
        f"```json\n{player.to_json()}\n```",
    )


@bot.command(name="jhoiuhasojnnaoiushjdoiasjdbgbgbgbgasdido")
async def performance(_: commands.Context):
    channel = await bot.fetch_channel(753007206122651741)
    await bot.send_by_host(
        channel,
        faceless_host,
        None,
        "O Formless, grant us but a sliver of your sight, that we might peer into the depths of the slip vicariously through you.",
        supress_thumbnail=True,
    )
    await asyncio.sleep(15.0)
    await bot.send_by_host(
        channel,
        schism,
        None,
        "`Request registered.`",
        supress_thumbnail=True,
    )
    await bot.send_by_host(
        channel,
        schism,
        None,
        "`datastream origin ꡕꡖꡟꡣꡂ... connected. opening kaathe.encyc.sim. subslip computation and heuristics integrated solution machine sucessfully transferred.`",
        supress_thumbnail=True,
    )
    await asyncio.sleep(6.0)
    await bot.send_by_host(
        channel,
        schism,
        None,
        "`auxilliary functions corrupted. integrity: 6.00%. initializing help screen.`",
        supress_thumbnail=True,
    )
    await asyncio.sleep(2.0)
    await bot.send_by_host(
        channel,
        schism,
        "Data Formats",
        """I understand multiple primitive data formats. A number is a series of digits, optionally delimited with underscores. `1` and `9_876_543` are both valid numbers. A sigil is a colon followed by a lowercase letter, then a mix of letters, numbers, and dashes. `:example` and `:more-Complex-example` are both valid sigils. A die is a quantity and a value separated by a case-insensitive 'd' character. When the quantity is 1, it is optional. `d20`, `8d6`, and `1D400` are all valid dice. A list is a collection of expressions separated by commas and surrounded by brackets. A list need not all be the same type. `[1, 3, d20, [2 + 6]]` is a valid list.""",
        supress_thumbnail=True,
    )
    await asyncio.sleep(2.0)
    await bot.send_by_host(
        channel,
        schism,
        "Operations",
        """I understand a single unary operation and four binary operations. To negate a number, you may prefix it with a dash as in `-4`. I can also add, subtract, multiply, and divide two separate expressions, as below:
```ocaml
1 + 2
d3 - 2
(d6 + 2) * 2
```""",
        supress_thumbnail=True,
    )
    await asyncio.sleep(2.0)
    await bot.send_by_host(
        channel,
        schism,
        "Identifiers and Variables",
        """A variable follows the same rules as a sigil, excepting the leading colon. `foo` and `bar-baz` are valid variable names. Variables can be assigned to with the `let <ident> = <expr> in` syntax. Below is an example of how one would assign multiple variables in the same request.
```ocaml
let x = 1 in
let y = 2 in
x + y
```
The example would resolve to `3`.""",
        supress_thumbnail=True,
    )
    await asyncio.sleep(2.0)
    await bot.send_by_host(
        channel,
        schism,
        "Functions",
        """A function may be used to decrease repetition in expression evaluation. To define a function, add an argument list after an identifier during its declaration, like so.
```ocaml
let subtract(first, second) = 
  first - second
in
subtract(5, 4)
```
The example would resolve to `1`.

Functions may also be defined anonymously with `|<arg list>| <expr>` syntax, as below.
```ocaml
let multiply = | first, second | first * second in
multiply(2, 6)
```
This would resolve to `12`.

Functions can hold multiple expressions in their body. The indentation is not necessary for me to understand what you've requested.
```ocaml
let strange(first, second) =
  let x = first - second in
  let y = first * second in
  x + y
in
strange(10, 3)
```
This resolves to `37`

Functions may also be passed in as arguments to other functions, and may be returned from functions.
```ocaml
let twice(f) = |x| f(x) + f(x) in
let plus-three-times-two = twice(|x| x + 3) in
plus-three-times-two(4)
```
This results in `14`""",
        supress_thumbnail=True,
    )
    await asyncio.sleep(2.0)
    await bot.send_by_host(
        channel,
        schism,
        "Rolling and Plotting",
        """A die or expression can be rolled and output in two ways. The first is when the final expression evaluates to a die, such as a message with a die and nothing else in it. `d20` will roll a twenty-sided die and `8d6` will roll eight six-sided dice. In addition, you may call the built-in `roll` function to queue up a roll once the expression finishes computing. You may chain multiple function calls together with the `;` operator.
```ocaml
let roll-twice(x) =
  roll(x); roll(x)
in
roll-twice(d20 + 5)
```
This would roll a single `d20 + 5` twice and print both to the output.

If you instead want to queue up multiple rolls, but wait to roll them until a later date, you may do so with the built-in `lazy-roll` function. This function expects any number of arguments, where each argument is a list of size two. The first element should be a sigil with the text of the die, and the second element should be the die to roll. As an example, here is an implementation of the multiple-attack penalty:
```ocaml
let map(die) =
    lazy-roll(
        [:first-attack, die],
        [:second-attack, die - 5],
        [:third-attack, die - 10]
    )
in
map(d20 + 11)
```
To plot the distribution of a given expression, use the `plot` built-in function. This will queue up a bar-graph of the given expression, and print it out to the output for your viewing pleasure.""",
        supress_thumbnail=True,
    )
    await asyncio.sleep(2.0)
    await bot.send_by_host(
        channel,
        schism,
        "Other Useful Builtins",
        """To take the greatest k of some set of dice, use the `take-highest` function. The `take-lowest` function operates similiarly. As an example, below is an implementation of disadvantage and rolling for stats:
```ocaml
let roll-for-stats() =
  take-highest(3, 4d6)
in
let disadvantage() = 
  take-lowest(1, 2d20)
in
roll-for-stats(); disadvantage()
```

To dynamically generate new dice, use the `d` function. If you pass in a number it will give back a die with that many sides. If you pass in a list it will make a die with those possiblilities as a result. If you pass in two arguments, the first argument will be the quantity of dice. `d(3)`, `d(2,6)`, `d([1, 2, 4])` and `d(d, [7, 8, 9])` are all valid calls.

To save a variable, expression, or function, use the `save` function. The first argument is a sigil of the name, and the second argument is the variable itself. `save(:adv, || take-highest(1, 2d20))` would save the `adv()` function for use across sessions.
""",
        supress_thumbnail=True,
    )
    await asyncio.sleep(6.0)
    await bot.send_by_host(
        channel,
        schism,
        None,
        "`explanation complete.`",
        supress_thumbnail=True,
    )


def run(token: str, db_path: Union[str, None]):
    bot.init_db(db_path)
    bot.run(token)
