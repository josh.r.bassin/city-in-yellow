import os
import sys
from astrologer import Astrologer
from dotenv import load_dotenv

from bot import run

load_dotenv()

DISCORD_BOT_TOKEN = os.getenv("DISCORD_BOT_TOKEN")
SQLITE_PATH = os.getenv("SQLITE_PATH")

if DISCORD_BOT_TOKEN is None:
    print("Couldn't find a discord token. Is DISCORD_BOT_TOKEN set?", file=sys.stderr)
    sys.exit(-1)

run(DISCORD_BOT_TOKEN, SQLITE_PATH)
