CREATE TYPE game_edition AS ENUM ('pathfinder_2e', 'dnd_5e', 'one_dnd');

CREATE TABLE campaigns (
  id SERIAL PRIMARY KEY,
  name VARCHAR(256) NOT NULL UNIQUE,
  edition game_edition NOT NULL,
  is_one_shot BOOLEAN DEFAULT TRUE NOT NULL
);

CREATE TABLE active_campaign (
  id SERIAL PRIMARY KEY,
  campaign_id INTEGER REFERENCES campaigns (id) ON DELETE CASCADE NOT NULL
);