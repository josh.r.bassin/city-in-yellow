CREATE TYPE dice_source AS ENUM ('discord', 'foundry');

CREATE TABLE dice (
  id SERIAL PRIMARY KEY,
  base INTEGER NOT NULL,
  value INTEGER NOT NULL,
  source dice_source NOT NULL,
  timestamp TIMESTAMP DEFAULT current_timestamp NOT NULL,
  player_id INTEGER REFERENCES players (id) ON DELETE CASCADE NOT NULL,
  blame_id INTEGER REFERENCES players (id) ON DELETE CASCADE NOT NULL
);