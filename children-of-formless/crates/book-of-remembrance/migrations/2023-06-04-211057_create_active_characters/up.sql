CREATE TABLE active_characters (
  player_id INTEGER REFERENCES players (id) ON DELETE CASCADE NOT NULL,
  campaign_id INTEGER REFERENCES campaigns (id) ON DELETE CASCADE NOT NULL,
  character_id INTEGER REFERENCES characters (id) ON DELETE CASCADE NOT NULL,
  PRIMARY KEY (player_id, campaign_id)
);