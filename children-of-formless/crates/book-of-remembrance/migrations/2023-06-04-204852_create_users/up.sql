CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  username VARCHAR(256) NOT NULL,
  snowflake VARCHAR(256) NOT NULL UNIQUE,
  is_admin BOOLEAN DEFAULT FALSE NOT NULL,
  player_id INTEGER REFERENCES players (id) ON DELETE CASCADE NOT NULL
);

CREATE INDEX users_snowflake_index ON users USING HASH (snowflake);