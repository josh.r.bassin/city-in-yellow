// @generated automatically by Diesel CLI.

pub mod sql_types {
    #[derive(diesel::query_builder::QueryId, diesel::sql_types::SqlType)]
    #[diesel(postgres_type(name = "dice_source"))]
    pub struct DiceSource;

    #[derive(diesel::query_builder::QueryId, diesel::sql_types::SqlType)]
    #[diesel(postgres_type(name = "game_edition"))]
    pub struct GameEdition;
}

diesel::table! {
    active_campaign (id) {
        id -> Int4,
        campaign_id -> Int4,
    }
}

diesel::table! {
    active_characters (player_id, campaign_id) {
        player_id -> Int4,
        campaign_id -> Int4,
        character_id -> Int4,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::GameEdition;

    campaigns (id) {
        id -> Int4,
        #[max_length = 256]
        name -> Varchar,
        edition -> GameEdition,
        is_one_shot -> Bool,
    }
}

diesel::table! {
    characters (id) {
        id -> Int4,
        #[max_length = 256]
        name -> Varchar,
        #[max_length = 64]
        class -> Varchar,
        #[max_length = 64]
        color -> Nullable<Varchar>,
        is_dm -> Bool,
        player_id -> Int4,
        campaign_id -> Int4,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::DiceSource;

    dice (id) {
        id -> Int4,
        base -> Int4,
        value -> Int4,
        source -> DiceSource,
        timestamp -> Timestamp,
        player_id -> Int4,
        blame_id -> Int4,
    }
}

diesel::table! {
    players (id) {
        id -> Int4,
        #[max_length = 56]
        name -> Varchar,
    }
}

diesel::table! {
    users (id) {
        id -> Int4,
        #[max_length = 256]
        username -> Varchar,
        #[max_length = 256]
        snowflake -> Varchar,
        is_admin -> Bool,
        player_id -> Int4,
    }
}

diesel::joinable!(active_campaign -> campaigns (campaign_id));
diesel::joinable!(active_characters -> campaigns (campaign_id));
diesel::joinable!(active_characters -> characters (character_id));
diesel::joinable!(active_characters -> players (player_id));
diesel::joinable!(characters -> campaigns (campaign_id));
diesel::joinable!(characters -> players (player_id));
diesel::joinable!(users -> players (player_id));

diesel::allow_tables_to_appear_in_same_query!(
    active_campaign,
    active_characters,
    campaigns,
    characters,
    dice,
    players,
    users,
);
