mod models;
mod schema;

pub mod db {
    use diesel::{
        r2d2::{ConnectionManager, Pool},
        PgConnection,
    };

    pub fn establish_connection(url: String) -> Pool<ConnectionManager<PgConnection>> {
        let manager = ConnectionManager::<PgConnection>::new(url);

        Pool::builder()
            .test_on_check_out(true)
            .build(manager)
            .expect("Could not build connection pool")
    }
}

pub mod player {
    pub use crate::models::Player;
    use crate::schema::players::dsl::*;
    use diesel::prelude::*;

    pub fn create(
        conn: &mut PgConnection,
        player_name: String,
    ) -> Result<Player, diesel::result::Error> {
        diesel::insert_into(players)
            .values(&name.eq(player_name))
            .get_result(conn)
    }

    pub fn get_by_id(
        conn: &mut PgConnection,
        player_id: i32,
    ) -> Result<Player, diesel::result::Error> {
        players
            .filter(id.eq(player_id))
            .select(Player::as_select())
            .first(conn)
    }

    pub fn get_by_name(
        conn: &mut PgConnection,
        player_name: String,
    ) -> Result<Player, diesel::result::Error> {
        players
            .filter(name.eq(player_name))
            .select(Player::as_select())
            .first(conn)
    }

    pub fn get_by_snowflake(
        conn: &mut PgConnection,
        player_snowflake: String,
    ) -> Result<Player, diesel::result::Error> {
        use crate::schema::users::dsl::*;

        players
            .inner_join(users)
            .filter(snowflake.eq(player_snowflake))
            .select(Player::as_select())
            .first(conn)
    }

    pub fn delete(conn: &mut PgConnection, player_id: i32) -> Result<usize, diesel::result::Error> {
        diesel::delete(players.filter(id.eq(player_id))).execute(conn)
    }
}

pub mod campaign {
    pub use crate::models::{Campaign, GameEdition};
    use crate::schema::campaigns::dsl::*;
    use diesel::prelude::*;

    pub fn create(
        conn: &mut PgConnection,
        campaign_name: String,
        campaign_edition: GameEdition,
        campaign_is_one_shot: bool,
    ) -> Result<Campaign, diesel::result::Error> {
        diesel::insert_into(campaigns)
            .values(&(
                name.eq(campaign_name),
                edition.eq(campaign_edition),
                is_one_shot.eq(campaign_is_one_shot),
            ))
            .get_result(conn)
    }

    pub fn get_by_id(
        conn: &mut PgConnection,
        campaign_id: i32,
    ) -> Result<Campaign, diesel::result::Error> {
        campaigns
            .filter(id.eq(campaign_id))
            .select(Campaign::as_select())
            .first(conn)
    }

    pub fn get_by_name(
        conn: &mut PgConnection,
        campaign_name: String,
    ) -> Result<Campaign, diesel::result::Error> {
        campaigns
            .filter(name.eq(campaign_name))
            .select(Campaign::as_select())
            .first(conn)
    }

    pub fn get_active(conn: &mut PgConnection) -> Result<Campaign, diesel::result::Error> {
        use crate::schema::active_campaign;

        active_campaign::dsl::active_campaign
            .inner_join(campaigns)
            .filter(active_campaign::dsl::id.eq(1))
            .select(Campaign::as_select())
            .first(conn)
    }

    pub fn delete(
        conn: &mut PgConnection,
        campaign_id: i32,
    ) -> Result<usize, diesel::result::Error> {
        diesel::delete(campaigns.filter(id.eq(campaign_id))).execute(conn)
    }
}

pub mod active_campaign {
    pub use crate::models::ActiveCampaign;
    use crate::schema::active_campaign::dsl::*;
    use diesel::prelude::*;

    pub fn set_active_campaign(
        conn: &mut PgConnection,
        active_campaign_id: i32,
    ) -> Result<usize, diesel::result::Error> {
        diesel::insert_into(active_campaign)
            .values(&(id.eq(1), campaign_id.eq(active_campaign_id)))
            .on_conflict(id)
            .do_update()
            .set(campaign_id.eq(active_campaign_id))
            .execute(conn)
    }
}

pub mod character {
    pub use crate::models::Character;
    use crate::schema::characters::dsl::*;
    use diesel::prelude::*;

    pub fn create(
        conn: &mut PgConnection,
        character_name: String,
        character_class: String,
        character_color: Option<String>,
        character_player_id: i32,
        character_campaign_id: i32,
    ) -> Result<Character, diesel::result::Error> {
        let character: Character = diesel::insert_into(characters)
            .values(&(
                name.eq(character_name),
                class.eq(character_class),
                color.eq(character_color),
                player_id.eq(character_player_id),
                campaign_id.eq(character_campaign_id),
            ))
            .get_result(conn)?;

        match crate::active_character::get_active_character(
            conn,
            character.player_id,
            character.campaign_id,
        ) {
            Err(err) => Err(err),
            Ok(Some(_)) => Ok(character),
            Ok(None) => {
                crate::active_character::set_active_character(
                    conn,
                    character.player_id,
                    character.campaign_id,
                    character.id,
                )?;
                Ok(character)
            }
        }
    }

    pub fn get_by_id(
        conn: &mut PgConnection,
        character_id: i32,
    ) -> Result<Character, diesel::result::Error> {
        characters
            .filter(id.eq(character_id))
            .select(Character::as_select())
            .first(conn)
    }

    pub fn get_by_name(
        conn: &mut PgConnection,
        character_name: String,
    ) -> Result<Character, diesel::result::Error> {
        characters
            .filter(name.eq(character_name))
            .select(Character::as_select())
            .first(conn)
    }

    pub fn get_by_snowflake(
        conn: &mut PgConnection,
        snowflake: String,
    ) -> Result<Character, diesel::result::Error> {
        use crate::schema::*;

        active_campaign::dsl::active_campaign
            .inner_join(
                active_characters::dsl::active_characters
                    .on(active_characters::dsl::campaign_id.eq(active_campaign::dsl::campaign_id)),
            )
            .inner_join(
                characters::dsl::characters
                    .on(characters::dsl::id.eq(active_characters::dsl::character_id)),
            )
            .inner_join(users::dsl::users.on(users::dsl::player_id.eq(characters::dsl::player_id)))
            .filter(active_campaign::dsl::id.eq(1))
            .filter(users::dsl::snowflake.eq(snowflake))
            .select(Character::as_select())
            .first(conn)
    }

    pub fn delete(
        conn: &mut PgConnection,
        character_id: i32,
    ) -> Result<usize, diesel::result::Error> {
        diesel::delete(characters.filter(id.eq(character_id))).execute(conn)
    }
}

pub mod active_character {
    pub use crate::models::ActiveCharacter;
    use crate::{character::Character, schema::active_characters::dsl::*};
    use diesel::prelude::*;

    pub fn set_active_character(
        conn: &mut PgConnection,
        active_player_id: i32,
        active_campaign_id: i32,
        active_character_id: i32,
    ) -> Result<usize, diesel::result::Error> {
        diesel::insert_into(active_characters)
            .values(&(
                player_id.eq(active_player_id),
                campaign_id.eq(active_campaign_id),
                character_id.eq(active_character_id),
            ))
            .on_conflict((player_id, campaign_id))
            .do_update()
            .set(character_id.eq(active_character_id))
            .execute(conn)
    }

    pub fn get_active_character(
        conn: &mut PgConnection,
        active_player_id: i32,
        active_campaign_id: i32,
    ) -> Result<Option<Character>, diesel::result::Error> {
        match active_characters
            .filter(player_id.eq(active_player_id))
            .filter(campaign_id.eq(active_campaign_id))
            .select(ActiveCharacter::as_select())
            .first(conn)
        {
            Err(diesel::result::Error::NotFound) => Ok(None),
            Err(err) => Err(err),
            Ok(ActiveCharacter {
                character_id: cid, ..
            }) => crate::character::get_by_id(conn, cid).map(Some),
        }
    }
}

pub mod user {
    pub use crate::models::User;
    use crate::schema::users::dsl::*;
    use diesel::prelude::*;

    pub fn create(
        conn: &mut PgConnection,
        user_name: String,
        user_snowflake: String,
        user_is_admin: bool,
        user_player_id: i32,
    ) -> Result<User, diesel::result::Error> {
        diesel::insert_into(users)
            .values(&(
                username.eq(user_name),
                snowflake.eq(user_snowflake),
                is_admin.eq(user_is_admin),
                player_id.eq(user_player_id),
            ))
            .get_result(conn)
    }

    pub fn get_by_snowflake(
        conn: &mut PgConnection,
        user_snowflake: String,
    ) -> Result<User, diesel::result::Error> {
        users
            .filter(snowflake.eq(user_snowflake))
            .select(User::as_select())
            .first(conn)
    }

    pub fn delete(conn: &mut PgConnection, user_id: i32) -> Result<usize, diesel::result::Error> {
        diesel::delete(users.filter(id.eq(user_id))).execute(conn)
    }
}

pub mod die {
    use std::time::SystemTime;

    pub use crate::models::{DiceSource, Die};
    use crate::schema::dice::dsl::*;
    use diesel::prelude::*;

    pub fn create(
        conn: &mut PgConnection,
        die_base: i32,
        die_value: i32,
        die_source: DiceSource,
        die_player_id: i32,
        die_blame_id: i32,
    ) -> Result<Die, diesel::result::Error> {
        diesel::insert_into(dice)
            .values(&(
                base.eq(die_base),
                value.eq(die_value),
                source.eq(die_source),
                player_id.eq(die_player_id),
                blame_id.eq(die_blame_id),
            ))
            .get_result(conn)
    }

    pub fn get_by_timespan(
        conn: &mut PgConnection,
        start_time: SystemTime,
        end_time: SystemTime,
    ) -> Result<Vec<Die>, diesel::result::Error> {
        dice.filter(timestamp.between(start_time, end_time))
            .select(Die::as_select())
            .load(conn)
    }

    pub fn get_all(conn: &mut PgConnection) -> Result<Vec<Die>, diesel::result::Error> {
        dice.select(Die::as_select()).load(conn)
    }

    pub fn delete(conn: &mut PgConnection, die_id: i32) -> Result<usize, diesel::result::Error> {
        diesel::delete(dice.filter(id.eq(die_id))).execute(conn)
    }
}
