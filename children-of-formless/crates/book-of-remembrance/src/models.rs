use diesel::prelude::*;
use std::time::SystemTime;

#[derive(Debug, Insertable, Identifiable, Selectable, Queryable)]
#[diesel(table_name = crate::schema::players)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Player {
    pub id: i32,
    pub name: String,
}

#[derive(Associations, Debug, Insertable, Identifiable, Selectable, Queryable)]
#[diesel(table_name = crate::schema::users)]
#[diesel(belongs_to(Player))]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct User {
    pub id: i32,
    pub username: String,
    pub snowflake: String,
    pub is_admin: bool,
    pub player_id: i32,
}

#[allow(non_camel_case_types)]
#[derive(Clone, Debug, diesel_derive_enum::DbEnum)]
#[ExistingTypePath = "crate::schema::sql_types::GameEdition"]
pub enum GameEdition {
    pathfinder_2e,
    dnd_5e,
    one_dnd,
}

#[derive(Clone, Debug, Insertable, Identifiable, Selectable, Queryable)]
#[diesel(table_name = crate::schema::campaigns)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Campaign {
    pub id: i32,
    pub name: String,
    pub edition: GameEdition,
    pub is_one_shot: bool,
}

#[derive(Associations, Debug, Insertable, Identifiable, Selectable, Queryable)]
#[diesel(table_name = crate::schema::active_campaign)]
#[diesel(belongs_to(Campaign))]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct ActiveCampaign {
    pub id: i32,
    pub campaign_id: i32,
}

#[derive(Associations, Debug, Insertable, Identifiable, Selectable, Queryable)]
#[diesel(table_name = crate::schema::characters)]
#[diesel(belongs_to(Player))]
#[diesel(belongs_to(Campaign))]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Character {
    pub id: i32,
    pub name: String,
    pub class: String,
    pub color: Option<String>,
    pub is_dm: bool,
    pub player_id: i32,
    pub campaign_id: i32,
}

#[derive(Associations, Debug, Insertable, Identifiable, Selectable, Queryable)]
#[diesel(table_name = crate::schema::active_characters)]
#[diesel(primary_key(player_id, campaign_id))]
#[diesel(belongs_to(Player))]
#[diesel(belongs_to(Campaign))]
#[diesel(belongs_to(Character))]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct ActiveCharacter {
    pub player_id: i32,
    pub campaign_id: i32,
    pub character_id: i32,
}

#[allow(non_camel_case_types)]
#[derive(Debug, diesel_derive_enum::DbEnum)]
#[ExistingTypePath = "crate::schema::sql_types::DiceSource"]
pub enum DiceSource {
    discord,
    foundry,
}

#[derive(Associations, Debug, Insertable, Identifiable, Selectable, Queryable)]
#[diesel(table_name = crate::schema::dice)]
#[diesel(belongs_to(Player))]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Die {
    pub id: i32,
    pub base: i32,
    pub value: i32,
    pub source: DiceSource,
    pub timestamp: SystemTime,
    pub player_id: i32,
    pub blame_id: i32,
}
