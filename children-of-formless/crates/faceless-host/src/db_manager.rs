use std::{
    collections::HashMap,
    time::{Duration, SystemTime},
};

use book_of_remembrance::{
    campaign::{self, Campaign},
    character, db,
    die::{self, Die},
    player, user,
};
use diesel::{
    r2d2::{ConnectionManager, Pool},
    PgConnection,
};
use rand::{Rng, SeedableRng};

#[derive(Clone, Debug)]
pub struct PlayerInfo {
    pub player_id: i32,
    pub player_name: String,
    pub discord_username: String,
    pub discord_snowflake: String,
    pub is_admin: bool,
    pub character_name: String,
    pub character_class: String,
    pub is_dm: bool,
}

#[derive(Clone, Debug)]
pub struct SeedInfo {
    pub seed: u64,
    pub blame: String,
    pub blame_id: i32,
}

pub struct DbManager {
    pool: Pool<ConnectionManager<PgConnection>>,
    player_info_cache: HashMap<String, PlayerInfo>,
    campaign_info_cache: Option<Campaign>,
    seed_info_cache: Option<SeedInfo>,
}

impl DbManager {
    pub fn new(url: &str) -> Self {
        Self {
            pool: db::establish_connection(url.to_owned()),
            player_info_cache: HashMap::new(),
            campaign_info_cache: None,
            seed_info_cache: None,
        }
    }

    pub fn get_player_info(
        &mut self,
        snowflake: &str,
    ) -> Result<PlayerInfo, diesel::result::Error> {
        if let Some(res) = self.player_info_cache.get(snowflake) {
            return Ok(res.clone());
        }

        let conn = &mut self.pool.get().unwrap();

        let player = player::get_by_snowflake(conn, snowflake.to_owned())?;
        let character = character::get_by_snowflake(conn, snowflake.to_owned())?;
        let user = user::get_by_snowflake(conn, snowflake.to_owned())?;

        let player_info = PlayerInfo {
            player_id: player.id,
            player_name: player.name,
            discord_username: user.username,
            discord_snowflake: user.snowflake,
            is_admin: user.is_admin,
            character_name: character.name,
            character_class: character.class,
            is_dm: character.is_dm,
        };

        self.player_info_cache
            .insert(snowflake.to_owned(), player_info.clone());
        Ok(player_info)
    }

    pub fn get_campaign_info(&mut self) -> Result<Campaign, diesel::result::Error> {
        if let Some(res) = &self.campaign_info_cache {
            return Ok(res.clone());
        }

        let conn = &mut self.pool.get().unwrap();
        let campaign_info = campaign::get_active(conn)?;

        self.campaign_info_cache = Some(campaign_info.clone());
        Ok(campaign_info)
    }

    pub fn gen_seed_info(&mut self, requester_name: &str, requester_id: i32) -> SeedInfo {
        let mut seed = rand::rngs::StdRng::from_entropy();
        let val: u64 = seed.gen();

        let seed_info = SeedInfo {
            seed: val,
            blame: requester_name.into(),
            blame_id: requester_id,
        };

        self.seed_info_cache = Some(seed_info.clone());
        seed_info
    }

    pub fn get_seed_info(&mut self, requester_name: &str, requester_id: i32) -> SeedInfo {
        if let Some(res) = &self.seed_info_cache {
            return res.clone();
        }

        self.gen_seed_info(requester_name, requester_id)
    }

    pub fn record_roll(
        &mut self,
        base: i32,
        value: i32,
        player_id: i32,
        blame_id: i32,
    ) -> Result<Die, diesel::result::Error> {
        let conn = &mut self.pool.get().unwrap();

        die::create(
            conn,
            base,
            value,
            die::DiceSource::discord,
            player_id,
            blame_id,
        )
    }

    pub fn get_week_dice(&mut self) -> Result<Vec<Die>, diesel::result::Error> {
        let conn = &mut self.pool.get().unwrap();

        die::get_by_timespan(
            conn,
            SystemTime::now()
                .checked_sub(Duration::from_secs(604800))
                .unwrap(),
            SystemTime::now(),
        )
    }

    pub fn get_all_dice(&mut self) -> Result<Vec<Die>, diesel::result::Error> {
        let conn = &mut self.pool.get().unwrap();

        die::get_all(conn)
    }
}
