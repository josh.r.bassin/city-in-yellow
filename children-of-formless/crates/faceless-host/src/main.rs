#![feature(async_closure)]

use std::{
    collections::HashMap,
    env,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
    time::Duration,
};

use book_of_remembrance::campaign::{Campaign, GameEdition};
use db_manager::{DbManager, SeedInfo};
use dotenvy::dotenv;
use message::{Host, Thumbnail};
use multimap::MultiMap;
use rand::{seq::SliceRandom, SeedableRng};
use schism::{Roll, RollKind, Rollable};
use serenity::{
    async_trait,
    builder::CreateComponents,
    futures::future::{join_all, Either},
    model::{
        application::interaction::InteractionResponseType,
        channel::Message,
        gateway::{Activity, Ready},
        prelude::{
            component::ButtonStyle, ChannelId, ChannelType, GuildId, ReactionType, ResumedEvent,
        },
        webhook::Webhook,
    },
    prelude::*,
};
use tokio::sync::MutexGuard;
use tracing::{event, Level};

use crate::db_manager::PlayerInfo;
use crate::message::{host_says, SendArgs};

mod db_manager;
mod message;

fn get_webhook(
    msg: &Message,
    webhooks: MutexGuard<HashMap<GuildId, HashMap<ChannelId, Webhook>>>,
) -> Option<Webhook> {
    let guild_id = match msg.guild_id {
        None => {
            event!(target: "faceless_host", Level::ERROR, "Could not find guild id in message: {:#?}.", msg);
            return None;
        }
        Some(guild_id) => guild_id,
    };

    let webhooks = match webhooks.get(&guild_id) {
        None => {
            event!(target: "faceless_host", Level::ERROR, "No known webhooks for guild: {:#?}.", guild_id);
            return None;
        }
        Some(webhooks) => webhooks,
    };

    let webhook = match webhooks.get(&msg.channel_id) {
        None => {
            event!(target: "faceless_host", Level::ERROR, "No known webhooks for channel: {:#?}.", &msg.channel_id);
            return None;
        }
        Some(webhook) => webhook,
    };

    Some(webhook.clone())
}

struct Handler {
    db: Arc<Mutex<DbManager>>,
    is_intialized: AtomicBool,
    webhooks: Arc<Mutex<HashMap<GuildId, HashMap<ChannelId, Webhook>>>>,
}

#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, ctx: Context, msg: Message) {
        if msg.is_own(&ctx.cache) || msg.author.bot {
            return;
        }

        let content = msg
            .content
            .trim()
            .trim_start_matches("```")
            .trim_start_matches("ocaml")
            .trim_end_matches("```")
            .trim()
            .to_owned();

        if content.is_empty() {
            return;
        }

        if content == "reseed()" {
            let seed_info = {
                let mut db = self.db.lock().await;

                let (requester_name, requester_id) = match db
                    .get_player_info(&msg.author.id.to_string())
                {
                    Ok(ok) => (ok.player_name, ok.player_id),
                    Err(e) => {
                        event!(target: "faceless_host", Level::ERROR, "error fetching player info: {:#?}.", e);
                        ("S.C.H.I.S.M.".into(), 6)
                    }
                };

                db.gen_seed_info(&requester_name, requester_id)
            };

            let webhook = {
                let webhooks = self.webhooks.lock().await;
                get_webhook(&msg, webhooks)
            };

            if let Some(webhook) = webhook {
                if let Err(err) = Host::Schism
                .send(
                    &ctx.http,
                    &webhook,
                    SendArgs {
                        title: Some(format!("{} reseeded!", seed_info.blame)),
                        contents: Some(vec![
                            "I've travelled _crosswick_ to a new ashmont space. All constants remain unchanged, except the seed.".to_owned(),
                            "I have bootstrapped a new universe. You have already been transported.".to_owned(),
                            "Task complete. Awaiting next instruction set.".to_owned(),
                        ].choose(&mut rand::rngs::StdRng::from_entropy()).unwrap().clone()),
                        fields: vec![(
                            "New Seed".to_owned(),
                            format!("{}", seed_info.seed),
                        )],
                        ..Default::default()
                    },
                )
                .await
            {
                event!(target: "faceless_host", Level::ERROR, "number send error: {:#?}.", err);
            }
            }

            return;
        };

        if content == "week-plot()" {
            let dice = {
                let mut db = self.db.lock().await;

                match db.get_week_dice() {
                    Ok(ok) => ok,
                    Err(err) => {
                        event!(target: "faceless_host", Level::ERROR, "dice get error: {:#?}.", err);
                        Vec::new()
                    }
                }
            };

            let dice = dice
                .into_iter()
                .filter(|x| x.base == 20)
                .map(|x| (x.player_id, x.value))
                .collect::<MultiMap<i32, i32>>();

            println!("{:#?}", dice);
            return;
        }

        if content == "all-plot()" {
            let dice = {
                let mut db = self.db.lock().await;

                match db.get_all_dice() {
                    Ok(ok) => ok,
                    Err(err) => {
                        event!(target: "faceless_host", Level::ERROR, "dice get error: {:#?}.", err);
                        Vec::new()
                    }
                }
            };

            let dice = dice
                .into_iter()
                .filter(|x| x.base == 20)
                .map(|x| (x.player_id, x.value))
                .collect::<MultiMap<i32, i32>>()
                .into_iter()
                .map(|(k, vs)| {
                    (
                        k,
                        vs.into_iter().fold(vec![0; 20], |mut v, x| {
                            v[(x as usize) - 1] += 1;
                            v
                        }),
                    )
                })
                .collect::<HashMap<i32, Vec<i32>>>();

            println!("{:#?}", dice);
            return;
        }

        let (campaign, player_info, seed_info) = {
            let mut db = self.db.lock().await;

            let campaign = match db.get_campaign_info() {
                Ok(ok) => ok,
                Err(e) => {
                    event!(target: "faceless_host", Level::ERROR, "error fetching campaign: {:#?}.", e);
                    Campaign {
                        id: -1,
                        name: "ttrpg".to_owned(),
                        edition: book_of_remembrance::campaign::GameEdition::pathfinder_2e,
                        is_one_shot: false,
                    }
                }
            };

            let player_info = match db.get_player_info(&msg.author.id.to_string()) {
                Ok(ok) => Either::Right(ok),
                Err(e) => {
                    event!(target: "faceless_host", Level::ERROR, "error fetching player info: {:#?}.", e);
                    Either::Left(msg.author.name.clone())
                }
            };

            let (requester_name, requester_id) = match &player_info {
                Either::Right(player) => (player.player_name.clone(), player.player_id),
                Either::Left(_) => ("S.C.H.I.S.M.".into(), 6),
            };

            let seed_info = db.get_seed_info(&requester_name, requester_id);

            (campaign, player_info, seed_info)
        };

        let roll = match schism::roll(content, vec![], &mut rand::rngs::StdRng::from_entropy()) {
            Err(err) => {
                event!(target: "faceless_host", Level::DEBUG, "error rolling dice: {:#?}.", err);
                return;
            }
            Ok(roll) => roll,
        };

        let webhook = {
            let webhooks = self.webhooks.lock().await;
            get_webhook(&msg, webhooks)
        };

        if let Some(webhook) = webhook {
            let ctx = Arc::new(ctx);
            let webhook = Arc::new(webhook);

            roll_eager(
                self.db.clone(),
                ctx.clone(),
                webhook.clone(),
                &roll.to_roll,
                &campaign,
                &player_info,
                &seed_info,
            )
            .await;

            for roll in &roll.to_roll_lazy {
                let mut res =
                    send_roll(&ctx, &webhook, roll, &campaign, &player_info, &seed_info).await;

                loop {
                    match res {
                        Reroll::Stop => break,
                        Reroll::Continue(roll) => {
                            res = send_roll(
                                &ctx,
                                &webhook,
                                &roll,
                                &campaign,
                                &player_info,
                                &seed_info,
                            )
                            .await
                        }
                    }
                }
            }
        }
    }

    async fn ready(&self, ctx: Context, ready: Ready) {
        event!(target: "faceless_host", Level::INFO, "Formless has granted me a new vessel. My current body is `{}`.", ready.user.name);

        ctx.set_activity(Activity::watching("eddies in the Slip"))
            .await;
    }

    async fn cache_ready(&self, ctx: Context, guilds: Vec<GuildId>) {
        if !self.is_intialized.load(Ordering::Relaxed) {
            for guild in guilds {
                let channels = ctx.cache.guild_channels(guild);

                match channels {
                    None => {
                        event!(target: "faceless_host", Level::ERROR, "I was unable to locate any channels in this guild: {}.", guild);
                    }
                    Some(channels) => {
                        for (_, channel) in channels {
                            if let ChannelType::Text = channel.kind {
                                match channel.webhooks(&ctx.http).await {
                                    Err(err) => {
                                        event!(target: "faceless_host", Level::ERROR, "I was unable to retrieve the webhooks: {}.", err)
                                    }
                                    Ok(webhooks) => {
                                        match webhooks
                                            .iter()
                                            .find(|webhook| webhook.token.is_some())
                                        {
                                            Some(webhook) => {
                                                let mut webhooks = self.webhooks.lock().await;

                                                let mut channel_map = match webhooks.remove(&guild)
                                                {
                                                    Some(map) => map,
                                                    None => HashMap::new(),
                                                };
                                                channel_map.insert(channel.id, webhook.clone());
                                                webhooks.insert(guild, channel_map);
                                            }
                                            None => {
                                                match channel
                                                    .create_webhook(&ctx.http, "faceless-host")
                                                    .await
                                                {
                                                    Err(err) => {
                                                        event!(target: "faceless_host", Level::ERROR, "I was unable to create the webhook: {}.", err)
                                                    }
                                                    Ok(webhook) => {
                                                        let mut webhooks =
                                                            self.webhooks.lock().await;

                                                        let mut channel_map =
                                                            match webhooks.remove(&guild) {
                                                                Some(map) => map,
                                                                None => HashMap::new(),
                                                            };
                                                        channel_map
                                                            .insert(channel.id, webhook.clone());
                                                        webhooks.insert(guild, channel_map);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            self.is_intialized.swap(true, Ordering::Relaxed);
            event!(target: "faceless_host", Level::INFO, "I am ready to serve, my liege.");
        }
    }

    async fn resume(&self, _: Context, _: ResumedEvent) {
        event!(target: "faceless_host", Level::INFO, "My vision, once clouded, has been restored.");
    }
}

pub(crate) enum RollGoodness {
    Crit,
    Good,
    Okay,
    Bad,
    Fumble,
}

impl RollGoodness {
    pub(crate) fn invert(&self) -> Self {
        match self {
            RollGoodness::Crit => RollGoodness::Fumble,
            RollGoodness::Good => RollGoodness::Bad,
            RollGoodness::Okay => RollGoodness::Okay,
            RollGoodness::Bad => RollGoodness::Good,
            RollGoodness::Fumble => RollGoodness::Crit,
        }
    }
}

fn goodness<T>(roll: &T) -> RollGoodness
where
    T: Rollable,
{
    if roll.value() == roll.min() {
        return RollGoodness::Fumble;
    }

    if roll.value() == roll.max() {
        return RollGoodness::Crit;
    }

    let norm_value = roll.value() - roll.min();
    let norm_max = roll.max() - roll.min();

    if norm_value < (norm_max / 3) {
        return RollGoodness::Bad;
    }

    if norm_value > (norm_max / 3 * 2) {
        return RollGoodness::Good;
    }

    RollGoodness::Okay
}

async fn roll_eager(
    db: Arc<Mutex<DbManager>>,
    ctx: Arc<Context>,
    webhook: Arc<Webhook>,
    rolls: &Vec<Roll>,
    campaign: &Campaign,
    player_info: &Either<String, PlayerInfo>,
    seed_info: &SeedInfo,
) {
    let mut threads = Vec::new();
    for roll in rolls {
        let ctx = ctx.clone();
        let webhook = webhook.clone();
        let roll = roll.clone();
        let campaign = campaign.clone();
        let player_info = player_info.clone();
        let seed_info = seed_info.clone();

        {
            let mut db = db.lock().await;

            for (base, value) in roll.dice() {
                match db.record_roll(
                    base as i32,
                    value as i32,
                    match &player_info {
                        Either::Left(_) => 6,
                        Either::Right(p) => p.player_id,
                    },
                    seed_info.blame_id,
                ) {
                    Ok(_) => (),
                    Err(e) => {
                        event!(target: "faceless_host", Level::ERROR, "couldn't record roll: {}.", e)
                    }
                };
            }
        };

        threads.push(tokio::spawn(async move {
            let mut res =
                send_roll(&ctx, &webhook, &roll, &campaign, &player_info, &seed_info).await;

            loop {
                match res {
                    Reroll::Stop => break,
                    Reroll::Continue(roll) => {
                        res = send_roll(&ctx, &webhook, &roll, &campaign, &player_info, &seed_info)
                            .await
                    }
                }
            }
        }))
    }

    join_all(threads).await;
}

enum Reroll {
    Stop,
    Continue(Roll),
}

async fn send_roll<T>(
    ctx: &Context,
    webhook: &Webhook,
    roll: &T,
    campaign: &Campaign,
    player_info: &Either<String, PlayerInfo>,
    seed_info: &SeedInfo,
) -> Reroll
where
    T: Rollable,
{
    match roll.kind() {
        RollKind::Number => {
            if let Err(err) = Host::Schism
                .send(
                    &ctx.http,
                    webhook,
                    SendArgs {
                        title: Some(format!("Calculated: {}", roll.value())),
                        contents: Some(vec![
                            "I've calculated the requested problem. Results enclosed below.".to_owned(),
                            "To ensure this was correct, I spun up three demiplanes with this as a universal constant. All three agreed.".to_owned(),
                            "Task complete. Awaiting next instruction set.".to_owned(),
                        ].choose(&mut rand::rngs::StdRng::from_entropy()).unwrap().clone()),
                        fields: vec![(
                            "Result".to_owned(),
                            format!("{} = `{}`", roll.text(), roll.value()),
                        )],
                        ..Default::default()
                    },
                )
                .await
            {
                event!(target: "faceless_host", Level::ERROR, "number send error: {:#?}.", err);
            };

            Reroll::Stop
        }
        RollKind::Eager | RollKind::Lazy(_) => {
            let goodness = goodness(roll);
            let (host, contents) = host_says(&goodness);

            let character_name = match player_info {
                Either::Left(name)
                | Either::Right(PlayerInfo {
                    character_name: name,
                    ..
                }) => name,
            };

            let title = match goodness {
                RollGoodness::Crit => format!("{}: {} (Crit!)", character_name, roll.value()),
                RollGoodness::Fumble => format!("{}: {} (Fumble!)", character_name, roll.value()),
                _ => format!("{}: {}", character_name, roll.value()),
            };

            let footer_goodness = match player_info {
                Either::Left(_) | Either::Right(PlayerInfo { is_dm: false, .. }) => goodness,
                Either::Right(PlayerInfo { is_dm: true, .. }) => goodness.invert(),
            };

            let footer = match footer_goodness {
                RollGoodness::Crit => {
                    format!(
                        "very good • {} (from {}, with love)",
                        seed_info.seed, seed_info.blame
                    )
                }
                RollGoodness::Fumble => {
                    format!("very bad • {} (blame {})", seed_info.seed, seed_info.blame)
                }
                RollGoodness::Good => {
                    format!("good • {} (thank {})", seed_info.seed, seed_info.blame)
                }
                RollGoodness::Bad => {
                    format!("bad • {} ({} did this)", seed_info.seed, seed_info.blame)
                }
                RollGoodness::Okay => {
                    format!("okay • {} (by {})", seed_info.seed, seed_info.blame)
                }
            };

            let thumbnail = match player_info {
                Either::Left(_) => Thumbnail::Default,
                Either::Right(PlayerInfo {
                    character_class, ..
                }) => match campaign.edition {
                    GameEdition::pathfinder_2e => {
                        if character_class.eq("dm") || character_class.eq("gm") {
                            Thumbnail::Default
                        } else {
                            Thumbnail::Url(format!(
                                "https://2e.aonprd.com/Images/Class/{}_Icon.png",
                                character_class
                            ))
                        }
                    }

                    GameEdition::dnd_5e | GameEdition::one_dnd => match character_class
                        .to_lowercase()
                        .as_str()
                    {
                        "artificer" => Thumbnail::Url("https://i.imgur.com/QXKeVeE.png".to_owned()),
                        "barbarian" => Thumbnail::Url("https://i.imgur.com/izeK1Py.png".to_owned()),
                        "bard" => Thumbnail::Url("https://i.imgur.com/SjD0TDy.png".to_owned()),
                        "cleric" => Thumbnail::Url("https://i.imgur.com/Ns4op2a.png".to_owned()),
                        "druid" => Thumbnail::Url("https://i.imgur.com/mVeBkwF.png".to_owned()),
                        "dm" | "gm" => Thumbnail::Url("https://i.imgur.com/bYriJqV.png".to_owned()),
                        "fighter" => Thumbnail::Url("https://i.imgur.com/VmOxMtI.png".to_owned()),
                        "monk" => Thumbnail::Url("https://i.imgur.com/1MzgkLc.png".to_owned()),
                        "paladin" => Thumbnail::Url("https://i.imgur.com/kBoheDD.png".to_owned()),
                        "ranger" => Thumbnail::Url("https://i.imgur.com/oaEAeoQ.png".to_owned()),
                        "rogue" => Thumbnail::Url("https://i.imgur.com/5Dy4qb5.png".to_owned()),
                        "sorcerer" => Thumbnail::Url("https://i.imgur.com/a6mkvD5.png".to_owned()),
                        "warlock" => Thumbnail::Url("https://i.imgur.com/z2BSBPm.png".to_owned()),
                        "wizard" => Thumbnail::Url("https://i.imgur.com/l3BqKMc.png".to_owned()),
                        _ => Thumbnail::Default,
                    },
                },
            };

            let retry = CreateComponents::default()
                .create_action_row(|r| {
                    r.create_button(|b| {
                        b.custom_id("retry")
                            .style(ButtonStyle::Success)
                            .emoji(ReactionType::Unicode("🔄".to_owned()))
                    })
                })
                .clone();

            match host
                .send(
                    &ctx.http,
                    webhook,
                    SendArgs {
                        title: Some(title),
                        contents: Some(contents),
                        fields: vec![(
                            "Result".to_owned(),
                            format!("{} = `{}`", roll.text(), roll.value()),
                        )],
                        img: thumbnail,
                        components: Some(retry),
                        footer: Some(footer),
                        ..Default::default()
                    },
                )
                .await
            {
                Err(err) => {
                    event!(target: "faceless_host", Level::ERROR, "roll send error: {:#?}.", err);
                    Reroll::Stop
                }
                Ok(None) => {
                    event!(target: "faceless_host", Level::INFO, "roll send no response");
                    Reroll::Stop
                }
                Ok(Some(msg)) => {
                    let interaction = match msg
                        .await_component_interaction(ctx)
                        .timeout(Duration::from_secs(60 * 3))
                        .await
                    {
                        Some(x) => x,
                        None => {
                            return Reroll::Stop;
                        }
                    };

                    if let Err(err) = interaction
                        .create_interaction_response(&ctx.http, |r| {
                            r.kind(InteractionResponseType::UpdateMessage)
                                .interaction_response_data(|x| x.set_components(Default::default()))
                        })
                        .await
                    {
                        event!(target: "faceless_host", Level::ERROR, "roll interaction response failed {:#?}", err);
                        return Reroll::Stop;
                    };

                    Reroll::Continue(roll.reroll())
                }
            }
        }
    }
}

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();

    let _ = dotenv();
    let token = env::var("DISCORD_TOKEN").expect("Expected a token in the environment");
    let url = env::var("DATABASE_URL").expect("Expected a db url in the environment");

    let db = DbManager::new(&url);

    let intents = GatewayIntents::non_privileged()
        | GatewayIntents::GUILD_MESSAGES
        | GatewayIntents::DIRECT_MESSAGES
        | GatewayIntents::MESSAGE_CONTENT;

    let mut client = Client::builder(&token, intents)
        .event_handler(Handler {
            db: Arc::new(Mutex::new(db)),
            is_intialized: AtomicBool::new(false),
            webhooks: Arc::new(Mutex::new(HashMap::new())),
        })
        .await
        .expect("Err creating client");

    if let Err(why) = client.start_autosharded().await {
        println!("Client error: {:?}", why);
    }
}
