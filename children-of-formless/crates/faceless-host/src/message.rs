// struct Host {
//     name: String,
//     img: String,
//     color: String,
// }

// hali = Host("Hali", "https://i.imgur.com/bWSBQBc.png", "#c41e3a")
// tenno = Host("Tenno", "https://i.imgur.com/TZzDA2f.png", "#7df9ff")
// formless = Host("Formless", "https://i.imgur.com/sIxjMYW.png", "#7b00ff")
// faceless_host = Host("Faceless Host", "https://i.imgur.com/ritZadr.png", "#ffffff")
// # Sub-slip Computation and Heuristics Integrated Solution Machine
// schism = Host("SCHISM", "https://i.imgur.com/EIucrAD.png", "#aaa9ad")

use rand::{seq::SliceRandom, Rng, SeedableRng};
use serenity::{
    builder::CreateComponents,
    http::Http,
    model::{
        prelude::{Embed, Message},
        webhook::Webhook,
    },
    utils::Color,
};

use crate::RollGoodness;

#[derive(Debug, Default)]
pub(crate) enum Thumbnail {
    #[default]
    Default,

    None,
    Url(String),
}

#[derive(Debug, Default)]
pub(crate) struct SendArgs {
    pub title: Option<String>,
    pub contents: Option<String>,
    pub img: Thumbnail,
    pub fields: Vec<(String, String)>,
    pub file: Option<String>,
    pub components: Option<CreateComponents>,
    pub footer: Option<String>,
}

#[derive(Debug)]
pub(crate) enum Host {
    Hali,
    Tenno,
    Formless,
    FacelessHost,
    Schism,
}

impl Host {
    pub(crate) fn name(&self) -> String {
        (match self {
            Self::Hali => "Hali",
            Self::Tenno => "Tenno",
            Self::Formless => "Formless",
            Self::FacelessHost => "Faceless Host",
            Self::Schism => "S.C.H.I.S.M.", // Sub-slip Computation and Heuristics Integrated Solution Machine
        })
        .to_owned()
    }

    pub(crate) fn image(&self) -> String {
        (match self {
            Self::Hali => "https://i.imgur.com/bWSBQBc.png",
            Self::Tenno => "https://i.imgur.com/TZzDA2f.png",
            Self::Formless => "https://i.imgur.com/sIxjMYW.png",
            Self::FacelessHost => "https://i.imgur.com/ritZadr.png",
            Self::Schism => "https://i.imgur.com/EIucrAD.png",
        })
        .to_owned()
    }

    pub(crate) fn color(&self) -> Color {
        (match self {
            Self::Hali => Color::new(0xC41E3A),
            Self::Tenno => Color::new(0x7DF9FF),
            Self::Formless => Color::new(0x7B00FF),
            Self::FacelessHost => Color::new(0xFFFFFF),
            Self::Schism => Color::new(0xAAA9AD),
        })
        .to_owned()
    }

    pub(crate) async fn send(
        &self,
        http: &Http,
        webhook: &Webhook,
        SendArgs {
            title,
            contents,
            img,
            fields,
            file,
            components,
            footer,
        }: SendArgs,
    ) -> Result<Option<Message>, serenity::Error> {
        let e = Embed::fake(|b| {
            let mut b = b;

            if let Some(title) = title {
                b = b.title(title);
            }

            if let Some(contents) = contents {
                b = b.description(contents);
            }

            b = b.color(self.color());

            match img {
                Thumbnail::None => (),
                Thumbnail::Default => b = b.thumbnail(self.image()),
                Thumbnail::Url(url) => b = b.thumbnail(url),
            }

            for (title, text) in fields {
                b = b.field(title, text, false);
            }

            if let Some(file) = file {
                b = b.image(file);
            }

            if let Some(footer) = footer {
                b = b.footer(|f| f.text(footer))
            }

            b
        });

        webhook
            .execute(http, components.is_some(), |b| {
                let mut b = b;

                b = b.embeds(vec![e]);
                b = b.username(self.name());
                b = b.avatar_url(self.image());

                if let Some(components) = components {
                    b = b.set_components(components);
                }

                b
            })
            .await
    }
}

pub(crate) fn host_says(goodness: &RollGoodness) -> (Host, String) {
    let mut rng = rand::rngs::StdRng::from_entropy();
    let (host, lines) = match rng.gen() {
        true => (
            Host::Hali,
            match goodness {
                RollGoodness::Crit => vec![
                    "I don't know how much you bribed _Fortune_ for this, but was it worth it?",
                    "For once, you've actually done something worthy of praise.",
                    "A crit! Much more exciting than your normal fare.",
                ],
                RollGoodness::Good => vec![
                    "I set the bar low, but you managed to pass it.",
                    "Even a broken clock rolls high twice a day.",
                    "This is a good roll, how uncharacteristic of you.",
                ],
                RollGoodness::Okay => vec![
                    "This is certainly one of the most rolls I've ever seen.",
                    "Average is better than I thought you would do.",
                    "I have to believe you're just not trying hard enough.",
                ],
                RollGoodness::Bad => vec![
                    "This roll is exactly in line with what I expect of you.",
                    "While unfortunate, this isn't exactly unprecedented.",
                    "Your suffering and my amusment are directly correlated.",
                ],
                RollGoodness::Fumble => vec![
                    "While I can't claim this is on purpose, I can claim it's hilarious.",
                    "I have absolutely no doubt you deserve this in some form.",
                    "I know they say size isn't everything, but you're really testing that.",
                ],
            },
        ),
        false => (
            Host::Tenno,
            match goodness {
                RollGoodness::Crit => vec![
                    "Your number is the biggest number there is!",
                    "One crit, served freshly stolen from _Despair_'s jaws.",
                    "Not just anyone could roll as high as you did!",
                ],
                RollGoodness::Good => vec![
                    "Honestly, you're doing great and I'm proud of you.",
                    "This is really good, you must be practicing your dice skills!",
                    "Out of all your rolls, this one is my new favorite.",
                ],
                RollGoodness::Okay => vec![
                    "Back home we used to call a roll like this 'okay'.",
                    "Hey, your luck's on the rise! Bet you'll do even better next time.",
                    "That could have been so much worse! You have to look on the bright side.",
                ],
                RollGoodness::Bad => vec![
                    "I think something's wrong with your dice.",
                    "I rolled this one a few times, it was always bad.",
                    "Sometimes _Fortune_ just isn't in your favor.",
                ],
                RollGoodness::Fumble => vec![
                    "I dropped the die on the floor and got this. Is it bad?",
                    "I tried really hard and got you a one!",
                    "Look on the bright side, life can't get worse!",
                ],
            },
        ),
    };

    (host, lines.choose(&mut rng).unwrap().to_owned().to_owned())
}
